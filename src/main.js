import Vue from 'vue'

//  自定义地图组件
import larkMap from 'lark-map'
import { setTheme as setLarkMapTheme } from 'lark-map'
import 'lark-map/dist/lark-map.css'

Vue.use(larkMap)
setLarkMapTheme({ themeName: 'theme-lark-green' })

//  自定义组件
import larkCommon from 'lark-common'
import 'lark-common/dist/lark-common.css'
import { setTheme as setLarkCommonTheme } from 'lark-common'

Vue.use(larkCommon)
setLarkCommonTheme({ themeName: 'theme-lark-green' })

//  主题
import larkTheme from 'lark-theme'
import { setTheme } from 'lark-theme'

Vue.use(larkTheme)

setTheme({ themeName: 'theme-lark-green' })

import VueCompositionAPI from '@vue/composition-api'

Vue.use(VueCompositionAPI)

import 'font-awesome/css/font-awesome.min.css'

import Cookies from 'js-cookie'

import Element from 'element-ui'
import './assets/styles/element-variables.scss'

import '@/assets/styles/index.scss' // global css
import '@/assets/styles/mc.scss' // mc css
import App from './App'
import store from './store'
import router from './router'
import permission from './directive/permission'

import './assets/icons' // icon
import './permission' // permission control
import { getDicts } from '@/api/system/dict/data'
import { getConfigKey } from '@/api/system/config'
import { parseTime, resetForm, addDateRange, selectDictLabel, selectDictLabels, download, handleTree } from '@/utils/mc'
import Pagination from '@/components/Pagination'
// 自定义表格工具扩展
import RightToolbar from '@/components/RightToolbar'
// 代码高亮插件
import hljs from 'highlight.js'
import 'highlight.js/styles/github-gist.css'

import dataV from '@jiaminghi/data-view'

Vue.use(dataV)

// 全局方法挂载
Vue.prototype.getDicts = getDicts
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.selectDictLabels = selectDictLabels
Vue.prototype.download = download
Vue.prototype.handleTree = handleTree

Vue.prototype.msgSuccess = function(msg) {
  this.$message({ showClose: true, message: msg, type: 'success' })
}

Vue.prototype.msgError = function(msg) {
  this.$message({ showClose: true, message: msg, type: 'error' })
}

Vue.prototype.msgInfo = function(msg) {
  this.$message.info(msg)
}

// 全局组件挂载
Vue.component('Pagination', Pagination)
Vue.component('RightToolbar', RightToolbar)

Vue.use(permission)
Vue.use(hljs.vuePlugin)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
