// import Layout from '@/views/industry'
import Layout from '@/views/industry'

const industryRouter = [
  {
    path: '/',
    component: Layout,
    redirect: '/industry',
    children: [
      {
        path: '/industry',
        component: (resolve) => require(['@/views/industry/homePage'], resolve),
        meta: { title: '数字种业-总览' },
        hidden: true
      },
      {
        path: '/ctl',
        component: (resolve) => require(['@/views/industry/clt/index'], resolve),
        meta: { title: '数字种业-资源收集' },
        hidden: true
      },
      {
        path: '/gpm',
        component: (resolve) => require(['@/views/industry/gpm/index'], resolve),
        meta: { title: '数字种业-种质保存' },
        hidden: true
      },
      {
        path: '/lab',
        component: (resolve) => require(['@/views/industry/lab/index'], resolve),
        meta: { title: '数字种业-鉴定评价' },
        hidden: true
      },
      {
        path: '/zza',
        component: (resolve) => require(['@/views/industry/lab/assay/zzAssay'], resolve),
        meta: { title: '数字种业-化验管理' },
        hidden: true
      },
      {
        path: '/zzs',
        component: (resolve) => require(['@/views/industry/lab/storage/zzStorage'], resolve),
        meta: { title: '数字种业-入库管理' },
        hidden: true
      },
      {
        path: '/pro',
        component: (resolve) => require(['@/views/industry/lab/labList/proManager'], resolve),
        meta: { title: '数字种业-专家管理' },
        hidden: true
      },
      {
        path: '/cer',
        component: (resolve) => require(['@/views/industry/lab/labList/cerManager'], resolve),
        meta: { title: '数字种业-资质管理' },
        hidden: true
      },
      {
        path: '/cpf',
        component: (resolve) => require(['@/views/industry/cpf/index'], resolve),
        meta: { title: '数字种业-种子生产' },
        hidden: true
      },
      {
        path: '/cpf/plantDetail',
        component: (resolve) => require(['@/views/industry/cpf/plantDetail/index'], resolve),
        meta: { title: '数字种业-种子生产' },
        hidden: true
      },
      {
        path: '/nur',
        component: (resolve) => require(['@/views/industry/nur/index'], resolve),
        meta: { title: '数字种业-资源圃' },
        hidden: true
      }
    ]
  }

]
export default industryRouter
