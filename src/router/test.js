const testRouter = [
  {
    path: '/test',
    component: (resolve) => require(['@/views/test/index'], resolve),
    hidden: true
  },
  {
    path: '/tt',
    component: (resolve) => require(['@/test/transitionTest'], resolve),
    hidden: true
  }
]
export default testRouter
