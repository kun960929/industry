import request from '@/utils/request'

// 查询资源库树
export function shelveList(query) {
  return request({
    url: '/gpm/shelve/list',
    method: 'get',
    params: query
  })
}

//新增资源库树node
export function addShelve(data) {
  return request({
    url: '/gpm/shelve',
    method: 'post',
    data: data
  })
}

// 删除货架
export function delShelve(id) {
  return request({
    url: '/gpm/shelve/' + id,
    method: 'delete'
  })
}

// 修改货架
export function updateShelve(data) {
  return request({
    url: '/gpm/shelve',
    method: 'put',
    data: data
  })
}

//货架详情统计
export function statistical(query) {
  return request({
    url: '/gpm/shelve/statistical',
    method: 'get',
    params: query
  })
}

//出库
export function outStorage(data) {
  return request({
    url: '/gpm/outStorage',
    method: 'post',
    data
  })
}

//查询专家详细信息
export function expertDetail(id) {
  return request({
    url: '/lab/expert/' + id,
    method: 'get'
  })
}

//查询鉴定列表
export function authenticateList(id) {
  return request({
    url: '/lab/clt/authenticate/' + id,
    method: 'get'
  })
}

//查询种子详情
export function storageList(query) {
  return request({
    url: '/gpm/storage/list',
    method: 'get',
    params: query
  })
}
