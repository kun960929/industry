import request from '@/utils/request'


// 查询卡片数据
export function cardData() {
  return request({
    url: '/home/data',
    method: 'get'
  })
}


//  查询表头
export function getHeader(queryType) {
  return request({
    url: `/home/header/${queryType}`,
    method: 'get'
  })
}


//  查询统计数据
export function getTable(queryType, query) {
  return request({
    url: `/home/table/${queryType}`,
    method: 'get',
    params: query
  })
}

