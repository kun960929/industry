import request from '@/utils/request'

//  实验室数据
export function labList(params) {
  return request({
    url: '/lab/info/list',
    method: 'get',
    params
  })
}

//  新增实验室数据
export function addLab(data) {
  return request({
    url: '/lab/info',
    method: 'post',
    data
  })
}

//  修改实验室数据
export function editLab(data) {
  return request({
    url: '/lab/info',
    method: 'put',
    data
  })
}

//  删除实验室数据
export function deleteLab(id) {
  return request({
    url: `/lab/info/${id}`,
    method: 'delete'
  })
}

//  学名数据
export function getName(name) {
  return request({
    url: `/system/plant/list/by/${name}`,
    method: 'get'
  })
}

//  科、属数据
export function qualification(type) {
  return request({
    url: `/system/plant/list/qualification/${type}`,
    method: 'get'
  })
}

//  专家数据
export function getProList(id) {
  return request({
    url: `/lab/expert/list`,
    method: 'get'
  })
}

// 专家列表带分页
export function getProListPage(params) {
  params.state = '0'
  return request({
    url: `/lab/expert/page/list`,
    method: 'get',
    params
  })
}

// 获取一个专家
export function getPro(id) {
  return request({
    url: `/lab/expert/${id}`,
    method: 'get'
  })
}

//  新增专家
export function addPro(data) {
  return request({
    url: `/lab/expert`,
    method: 'post',
    data
  })
}

//  修改专家
export function editPro(data) {
  return request({
    url: `/lab/expert/`,
    method: 'put',
    data
  })
}

//  删除专家
export function deletePro(id) {
  return request({
    url: `/lab/expert/${id}`,
    method: 'delete'
  })
}

//  鉴定
export function autAdd(data) {
  return request({
    url: `/lab/clt/authenticate/add`,
    method: 'post',
    data
  })
}

//  获取化验数据
export function getAutListByCltId(params) {
  return request({
    url: `/lab/clt/germplasm/list`,
    method: 'get',
    params
  })
}

//  获取唯一化验数据
export function getAutData(id) {
  return request({
    url: `/lab/clt/${id}`,
    method: 'get'
  })
}

//  新增化验数据
export function addAutData(data) {
  return request({
    url: `/lab/clt`,
    method: 'post',
    data
  })
}

//  修改化验数据
export function editAutData(data) {
  return request({
    url: `/lab/clt`,
    method: 'put',
    data
  })
}

//  导出数据
export function exportAutData(params) {
  return request({
    url: `/lab/qualification/export`,
    method: 'get',
    params
  })
}

//  获取入库数据
export function getStorageList(params) {
  return request({
    url: `/gpm/inStorage/list`,
    method: 'get',
    params
  })
}

//  获取唯一入库数据
export function getStorageData(id) {
  return request({
    url: `/gpm/inStorage/${id}`,
    method: 'get'
  })
}

//  新增入库数据
export function addStorageData(data) {
  return request({
    url: `/gpm/inStorage`,
    method: 'post',
    data
  })
}

//  修改入库数据
export function editStorageData(data) {
  return request({
    url: `/gpm/inStorage`,
    method: 'put',
    data
  })
}

//  生成二维码
export function qrCode(params) {
  return request({
    url: `/lab/clt/qrCode`,
    method: 'get',
    params
  })
}

//  资质数据
export function getQuaList(params) {
  return request({
    url: `/lab/qualification/list`,
    method: 'get',
    params
  })
}

//  新增资质
export function addQua(data) {
  return request({
    url: `/lab/qualification`,
    method: 'post',
    data
  })
}

//  修改资质
export function editQua(data) {
  return request({
    url: `/lab/qualification/`,
    method: 'put',
    data
  })
}

//  删除资质
export function deleteQua(id) {
  return request({
    url: `/lab/qualification/${id}`,
    method: 'delete'
  })
}

//  获取实验室所有的图片
export function getLabImage(id) {
  return request({
    url: `/lab/info/get/photo/${id}`,
    method: 'get'
  })
}
