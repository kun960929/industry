import request from '@/utils/request'

// 查询采集任务
export function taskList(params) {
  return request({
    url: '/mapper/clt/task/list',
    method: 'get',
    params
  })
}

/**
 * 新增采集任务
 * @param params
 * @returns {AxiosPromise}
 */
export function addTask(data) {
  return request({
    url: '/mapper/clt/task/add',
    method: 'post',
    data
  })
}

/**
 * 修改采集任务
 * @param params
 * @returns {AxiosPromise}
 */
export function editTask(data) {
  return request({
    url: '/mapper/clt/task/edit',
    method: 'post',
    data
  })
}

/**
 * 禁用采集任务
 * @param id
 * @returns {AxiosPromise}
 */
export function disableTask(id) {
  return request({
    url: '/mapper/clt/task/disable/' + id,
    method: 'get'
  })
}

/**
 * 禁用采集任务
 * @param id
 * @returns {AxiosPromise}
 */
export function getTask(id) {
  return request({
    url: '/mapper/clt/task/' + id,
    method: 'get'
  })
}

/**
 * 查询采集信息
 * @param params
 * @returns {AxiosPromise}
 */
export function resultByTaskId(data) {
  return request({
    url: '/mapper/clt/resultByTaskId',
    method: 'get',
    params: data
  })
}

/**
 * 查询采集信息
 * @param params
 * @returns {AxiosPromise}
 */
export function resultList(data) {
  return request({
    url: '/mapper/clt/result/list',
    method: 'get',
    params: data
  })
}

/**
 * 增加采集信息
 * @param taskType
 * @param data
 * @returns {AxiosPromise}
 */
export function addResult(taskType, data) {
  return request({
    url: '/mapper/clt/add/result/' + taskType,
    method: 'post',
    data
  })
}

/**
 * 获取当前采集信息
 * @param params
 * @returns {AxiosPromise}
 */
export function realResult(params) {
  return request({
    url: '/mapper/clt/real/result',
    method: 'post',
    data: params
  })
}

export function resultById(id) {
  return request({
    url: '/mapper/clt/result/id/' + id,
    method: 'get'
  })
}

/**
 * 查询在线用户
 * @returns {AxiosPromise}
 */
export function onlineUser() {
  return request({
    url: '/mapper/clt/online/position',
    method: 'get'
  })
}

/**
 * 查询采集总体统计
 * @returns {AxiosPromise}
 */
export function statistics() {
  return request({
    url: '/mapper/clt/statistics',
    method: 'get'
  })
}

/**
 * 统计用户排名
 * @param params
 * @returns {AxiosPromise}
 */
export function statisticsRank(params) {
  return request({
    url: '/mapper/clt/statistics/rank',
    method: 'get',
    params
  })
}

/**
 * 统计采集种子
 * @param params
 * @returns {AxiosPromise}
 */
export function statisticsSpecies(params) {
  return request({
    url: '/mapper/clt/statistics/species',
    method: 'get',
    params
  })
}

//  获取轨迹数据
export function getTack(query) {
  return request({
    url: '/mapper/clt/online/position/list',
    method: 'get',
    params: query
  })
}

//  获取当前轨迹播放的用户信息
export function getCurrentUser(id) {
  return request({
    url: `/system/user/get/nickname/${id}`,
    method: 'get'
  })
}
