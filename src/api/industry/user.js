import request from '@/utils/request'

// 查寻用户列表
export function userList(params) {
  return request({
    url: '/mapper/clt/task/user',
    method: 'get',
    params
  })
}


