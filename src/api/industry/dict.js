import request from '@/utils/request'

// 查询
export function areaDict(params) {
  return request({
    url: '/system/area/list/' + params,
    method: 'get'
  })
}

// /system/area/list/province
