import request from '@/utils/request'

// 查询基地列表
export function selectBase(query) {
  query.status = '0'
  return request({
    url: '/fil/base/list',
    method: 'get',
    params: query
  })
}

export function deleteBase(id) {
  return request({
    url: `/fil/base/${id}`,
    method: 'delete'
  })
}

export function updateBase(data) {
  return request({
    url: `/fil/base`,
    method: 'put',
    data
  })
}

export function insertBase(data) {
  return request({
    url: `/fil/base`,
    method: 'post',
    data
  })
}

export function getInfo(id) {
  return request({
    url: `/fil/info/${id}`,
    method: 'get'
  })
}

export function selectInfo(query) {
  return request({
    url: '/fil/info/list',
    method: 'get',
    params: query
  })
}

export function deleteInfo(id) {
  return request({
    url: `/fil/info/${id}`,
    method: 'delete'
  })
}

export function updateInfo(data) {
  return request({
    url: `/fil/info`,
    method: 'put',
    data
  })
}


export function updateFilInfo(data) {
  return request({
    url: `/fil/info/update`,
    method: 'put',
    data
  })
}

export function insertInfo(data) {
  return request({
    url: `/fil/info`,
    method: 'post',
    data
  })
}

//播种情况查询
export function sowList(params) {
  return request({
    url: `/fil/sow/list`,
    method: 'get',
    params
  })
}

//播种情况编辑
export function sowEdit(data) {
  return request({
    url: `/fil/sow`,
    method: 'post',
    data
  })
}

//查询物候期
export function phaseList(params) {
  return request({
    url: `/fil/phase/list`,
    method: 'get',
    params
  })
}

//播种情况编辑
export function phaseEdit(data) {
  return request({
    url: `/fil/phase`,
    method: 'post',
    data
  })
}

export function manureList(params) {
  return request({
    url: `/fil/manure/list`,
    method: 'get',
    params
  })
}

export function manureEdit(data) {
  return request({
    url: `/fil/manure`,
    method: 'post',
    data
  })
}

export function wateringList(params) {
  return request({
    url: `/fil/watering/list`,
    method: 'get',
    params
  })
}

export function wateringEdit(data) {
  return request({
    url: `/fil/watering`,
    method: 'post',
    data
  })
}

export function weedingList(params) {
  return request({
    url: `/fil/weeding/list`,
    method: 'get',
    params
  })
}

export function weedingEdit(data) {
  return request({
    url: `/fil/weeding`,
    method: 'post',
    data
  })
}

export function fertilizerList(params) {
  return request({
    url: `/fil/fertilizer/list`,
    method: 'get',
    params
  })
}

export function fertilizerEdit(data) {
  return request({
    url: `/fil/fertilizer`,
    method: 'post',
    data
  })
}

export function pestsList(params) {
  return request({
    url: `/fil/pests/list`,
    method: 'get',
    params
  })
}

export function pestsEdit(data) {
  return request({
    url: `/fil/pests`,
    method: 'post',
    data
  })
}

export function harvestList(params) {
  return request({
    url: `/fil/harvest/list`,
    method: 'get',
    params
  })
}

export function harvestEdit(data) {
  return request({
    url: `/fil/harvest`,
    method: 'post',
    data
  })
}

export function conditionsList(params) {
  return request({
    url: `/fil/conditions/list`,
    method: 'get',
    params
  })
}

export function conditionsEdit(data) {
  return request({
    url: `/fil/conditions`,
    method: 'post',
    data
  })
}

export function machingList(params) {
  return request({
    url: `/fil/maching/list`,
    method: 'get',
    params
  })
}

export function machingEdit(data) {
  return request({
    url: `/fil/maching`,
    method: 'post',
    data
  })
}

export function centerList(params) {
  return request({
    url: `/fil/center/list`,
    method: 'get',
    params
  })
}




