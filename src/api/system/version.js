import request from '@/utils/request'

// 查询app版本信息 列表
export function listVersion(query) {
  return request({
    url: '/system/version/list',
    method: 'get',
    params: query
  })
}

// 查询app版本信息 详细
export function getVersion(appId) {
  return request({
    url: '/system/version/' + appId,
    method: 'get'
  })
}

// 新增app版本信息 
export function addVersion(data) {
  return request({
    url: '/system/version',
    method: 'post',
    data: data
  })
}

// 修改app版本信息 
export function updateVersion(data) {
  return request({
    url: '/system/version',
    method: 'put',
    data: data
  })
}

// 删除app版本信息 
export function delVersion(appId) {
  return request({
    url: '/system/version/' + appId,
    method: 'delete'
  })
}

// 导出app版本信息 
export function exportVersion(query) {
  return request({
    url: '/system/version/export',
    method: 'get',
    params: query
  })
}