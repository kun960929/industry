import request from '@/utils/request'

// 查询采种田列表
export function listField(query) {
  return request({
    url: '/field/field/list',
    method: 'get',
    params: query
  })
}

// 查询采种田详细
export function getField(id) {
  return request({
    url: '/field/field/' + id,
    method: 'get'
  })
}

// 新增采种田
export function addField(data) {
  return request({
    url: '/field/field',
    method: 'post',
    data: data
  })
}

// 修改采种田
export function updateField(data) {
  return request({
    url: '/field/field',
    method: 'put',
    data: data
  })
}

// 删除采种田
export function delField(id) {
  return request({
    url: '/field/field/' + id,
    method: 'delete'
  })
}

// 导出采种田
export function exportField(query) {
  return request({
    url: '/field/field/export',
    method: 'get',
    params: query
  })
}