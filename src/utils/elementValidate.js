export const baseValidate = (rule, value, callback) => {
  if (rule.type && rule.type == 'array') {
    arrayValidate(rule, value, callback)
    return
  }
  let { min = null, max = null, require = false, name = '此项' } = rule

  if (require) {
    if (value == null || value === '') {
      callback(new Error(`${name}不能为空`))
      return
    }
  }
  if (min != null) {
    if (value == null || value === '' || value.length < min) {
      callback(new Error(`${name}长度不能小于${min}`))
      return
    }
  }
  if (max != null) {
    if (value.length > max) {
      callback(new Error(`${name}长度不能大于${max}`))
      return
    }
  }

  callback()
}
const arrayValidate = (rule, value, callback) => {
  let { min = null, max = null, require = false, name = '此项' } = rule

  if (require) {
    if (value == null || value === '' || value.length == 0) {
      callback(new Error(`${name}不能为空`))
      return
    }
  }
  callback()
}
