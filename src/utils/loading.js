import { Loading } from 'element-ui'

export default (targetDom, text) => {
  const options = {
    body: true,
    fullscreen: true,
    // text: text && text !== '' ? text : '加载中...',
    // background: 'rgba(0,0,0,0.3)'
  }
  if (targetDom) {
    options.target = targetDom
  }
  return Loading.service(options)

}
