const homeMessage = {
  state: {
    title: '',
    headData: [
      {
        icon: 'el-icon-switch-button',
        label: '专题展示',
        value: 'ztzs',
        path: '/industry'
      },
      {
        icon: 'el-icon-switch-button',
        label: '后台管理',
        value: 'htgl',
        path: '/manager'
      }
    ]
  },

  mutations: {
    SET_TITLE: (state, title) => {
      state.title = title
    },
    SET_HEAD_DATA: (state, headData) => {
      state.headData = headData
    }
  },

  actions: {}
}

export default homeMessage
